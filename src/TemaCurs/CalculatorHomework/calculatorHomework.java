import org.junit.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.siit.curs7.Calculator;

public class calculatorHomework {

        static Calculator c;

        @BeforeClass
        public static void beforeTest(){
        c = new Calculator();
    }


        @Test
        public void testSum01(){
//        System.out.println(c.compute(2, 7, "+"));
            Assert.assertEquals(9, c.compute(2, 7, "+"), 0);


            }



        @Test
        public void testSum02(){
            Assert.assertEquals(-13482, c.compute(2164, -15646, "+"), 0);
        }

        @Test
        public void testSum03(){
            Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
        }

        @Test
        public void TestDiff01(){
            Assert.assertEquals(900,c.compute(1000, 100, "-"), 0);
        }

        @Test
        public void TestProduct01(){
            Assert.assertEquals(121, c.compute(11, 11, "*"), 0);
        }

        @Test(expected = IllegalArgumentException.class)
        public void TestUsupp01(){
            Assert.assertEquals(121, c.compute(11, 11, "x"), 0);
        }

        @Test
        public void TestDiv01(){
            Assert.assertEquals(2, c.compute(10, 5, "/"), 0);
        }

        @Test
        public void TestDiv02(){
            Assert.assertEquals(3.33, c.compute(10, 3, "/"), 0.01);
        }

        // This is the correct way to test if an exception is expedted to be raised
        @Test(expected = IllegalArgumentException.class)
        public void TestDiv03(){
            Assert.assertEquals(0,c.compute(10, 0, "/"), 0);
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test
    public void testSum12(){
    Assert.assertEquals(-52222, c.compute(3333, -55555, "+"), 0);
}

    @Test
    public void testSum13(){
        Assert.assertEquals(10, c.compute(9, 1, "+"), 0);
    }

    @Test
    public void TestUnlike11(){
        Assert.assertEquals(407,c.compute(432, 25, "-"), 0);
    }

    @Test
    public void TestObj11(){
        Assert.assertEquals(4096, c.compute(64, 64, "*"), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestMade11(){
        Assert.assertEquals(4096, c.compute(64, 64, "x"), 0);
    }

    @Test
    public void TestTest21(){
        Assert.assertEquals(7.5, c.compute(30, 4, "/"), 0);
    }

    @Test
    public void TestTest22(){
        Assert.assertEquals(4.166666666666667, c.compute(5, 1.2, "/"), 0.01);
    }

    @Test
    public void testSum31(){
        Assert.assertEquals(1995, c.compute(2000, -5, "+"), 0);
    }

    @Test
    public void testSum32(){
        Assert.assertEquals(200, c.compute(199, 1, "+"), 0);
    }

    @Test
    public void TestUnlike33(){
        Assert.assertEquals(4000,c.compute(6000, 2000, "-"), 0);
    }

    @Test
    public void Test33(){
        Assert.assertEquals(9, c.compute(3, 3, "*"), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestMade33(){
        Assert.assertEquals(4, c.compute(2, 2, "x"), 0);
    }

    @Test
    public void TestTest34(){
        Assert.assertEquals(7.5, c.compute(30, 4, "/"), 0);
    }

    @Test
    public void TestTest35(){
        Assert.assertEquals(15.625, c.compute(50, 3.2, "/"), 0.01);
    }


    @AfterClass
    public static void afterTest() {
        c = new Calculator();


    }




}





