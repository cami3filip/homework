package TemaCurs.temaCurs3;

public class PrimeNumbers {
    public static void main(String[] args) {


        int limit = 1000000;
        int count=1;
        System.out.println("Printing prime number from 1 to " + limit);
        for(int number = 2; number <= limit; number++)
        {
            //print prime numbers only
            if(isPrime(number)){
                System.out.println(number);
                count++;

                //if(number == limit)
                //{System.out.println("Count: " + count);}
            }
            if(number == limit)
            {System.out.println("total  of prime numbers in " +limit+ " is: " + count);}

        }

    }


    public static boolean isPrime(int number){
        for(int i=2; i<number; i++){
            if(number%i == 0){
                return false; //number is divisible so its not prime
            }
        }
        return true; //number is prime now
    }

}
