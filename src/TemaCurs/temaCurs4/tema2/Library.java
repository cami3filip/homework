package TemaCurs.temaCurs4.tema2;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Scanner;

class   Library {
    public static void main(String[] args) {


        Author autor = new Author("Vasile", "Vasile@me.com");

        Book carte = new Book("Tupeu de poet",1900,autor,30.00);

        //System.out.print(carte.toString());
        String cartea = carte.getName();

        System.out.print("Book: " + cartea + " (" + carte.getPrice()+" RON), by " + carte.getAuthor().getName()+ ", published in " + carte.getYear());
    }

}
