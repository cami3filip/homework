package TemaCurs.temaCurs4.tema2;

public class Book {

    //private variables
    private String name;
    private int year;
    private Author author;
    private double price;



    //constructor
    public Book(String name, int year, Author author,double price){
        this.author= author;
        this.name = name;
        this.year=year;
        this.price = price;
    }

    //getters

    public String getName(){
        return name;
    }

    public Author getAuthor(){
        return this.author;
    }

    public double getPrice(){
        return price;
    }

    public int getYear(){
        return year;
    }

    //setters
    public void setAuthor(Author newAuthor){
        author=newAuthor;
    }
    public void setName (String newName){
        this.name=newName;
    }
    public void setYear(int newYear){
        this.year=newYear;
    }
    public void setPrice(double newPrice){
        this.price= newPrice;
    }

    public String toString(){
        return "Name: " + this.name + "\n"+"Nume Author: " + this.author.getName() + "\n" +
                "year: " + this.year + "\n" + "Price: " + this.price;
    }
}
