package TemaCurs.temaCurs4.tema2;

public class Author {
    //private variable
    private String name;
    private String email;

    //constructor
    public Author(String name, String email) {
        this.name = name;
        this.email = email;
    }

    //getters
    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String newName) {
        this.name= newName;
    }

    public void setEmail(String newEmail) {
        this.email= newEmail;
    }

}
